const { obtenerProceso } = require('../../infrastructura/database/Postgres');

class ObtenerControlador {
    constructor(data) {
        this.data = data;
    }

    obtenerElemento() {
        return obtenerProceso();
    }
}

module.exports = ObtenerControlador;