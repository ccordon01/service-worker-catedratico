const { crearProceso } = require('../../infrastructura/database/Postgres');

class CrearControlador {
    constructor(data) {
        this.data = data;
    }

    crearElemento() {
        return crearProceso(this.data);
    }
}

module.exports = CrearControlador;