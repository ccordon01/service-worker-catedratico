// Importar los controladores para procesar cada entrada del servicio.
const CrearControlador = require('./CrearControlador');
const ObtenerControlador = require('./ObtenerControlador');

class MasterControlador {
    procesarMensaje(entrada) {
        const mensaje = JSON.parse(entrada);
        switch (mensaje.accion) {
            case 'crear':
                const crearControlador = new CrearControlador(mensaje.data);
                return crearControlador.crearElemento();
            case 'obtener':
                const obtenerControlador = new ObtenerControlador();
                return obtenerControlador.obtenerElemento();
            default:
                break;
        }
    }
}

module.exports = MasterControlador;
