require('dotenv').config();
const WebSocket = require('ws');
const MasterControlador = require('./interfaz/controladores/MasterControlador');

const wss = new WebSocket.Server({ port: process.env.SERVICE_WORKER });
const masterControlador = new MasterControlador();

wss.on('connection', (ws) => {
    ws.on('message', async (message) => {
        console.log('received: %s', message);
        try {
            const result = await masterControlador.procesarMensaje(message);
            ws.send(JSON.stringify(result));
        } catch (error) {
            ws.send('error');
        }
        // ws.send('something');
        // ws.terminate();
    });
});


wss.on('close', () => {

});