const { Pool } = require('pg');

const pool = new Pool();

module.exports = {
    crearProceso: async (data) => {
        const client = await pool.connect();
        try {
            const res = await client.query(`CALL "insertar_Catedratico"($1::text , 
                TO_DATE($2,'DD/MM/YYYY')::date, $3, $4, $5::text, $6::text, $7, $8)`
                , [
                    data.catedratico.nombre
                    , data.catedratico.fechaNacimiento
                    , data.catedratico.codigoIdentidad
                    , data.catedratico.tipoCatedratico
                    , data.usuario.nombreUsuario
                    , data.usuario.claveUsuario
                    , 1
                    , data.catedratico.idInstitucionEducativa
                ]);
            return Promise.resolve(res.rows[0]);
        }
        catch (error) {
            console.error(error)
            return Promise.reject(error);
        }
        finally {
            // Make sure to release the client before any error handling,
            // just in case the error handling itself throws an error.
            client.release();
        }
    },
    obtenerProceso: async (data) => {
        const client = await pool.connect();
        try {
            const res = await client.query('SELECT * FROM "obtener_catedraticos";')
            return Promise.resolve(res.rows);
        }
        catch (error) {
            return Promise.reject(error);
        }
        finally {
            // Make sure to release the client before any error handling,
            // just in case the error handling itself throws an error.
            client.release();
        }
    }
};