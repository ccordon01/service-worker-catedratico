FROM node:current-buster-slim

# Create app directory
WORKDIR /usr/src/service

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .
ENV PGUSER=postgres
ENV PGPASSWORD=wu4ce5fiAAy4guC8
ENV PGHOST=130.211.193.235
ENV PGDATABASE=clickid-db
ENV PGPORT=5432
ENV SERVICE_WORKER=8080
EXPOSE 8080
CMD [ "node", "./src/service.js" ]